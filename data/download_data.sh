#!/bin/zsh

dvc run -f download_zip.dvc \
  -o zips/ \
  kaggle competitions download -c mens-machine-learning-competition-2019 --file DataFiles.zip --path zips/

dvc run -f extract_zips.dvc -o prepared/ unzip zips/DataFiles.zip -d prepared
