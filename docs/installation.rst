.. highlight:: shell

============
Installation
============


From sources
------------

The sources for sportsball can be downloaded from the `GitLab repo`_.

You can clone the public repository:

.. code-block:: console

    $ git clone git://github.com/dshurick/sportsball

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://gitlab.com/dshurick/sportsball
.. _tarball: https://gitlab.com/dshurick/sportsball/-/archive/feature/ncaamb-compile-data/sportsball-feature-ncaamb-compile-data.tar
