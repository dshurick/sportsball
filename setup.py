#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "Click>=6.0",
    "pandas",
    "beautifulsoup4",
    "lxml",
    "requests",
    "python-dateutil",
    "smart-open",
    "sqlalchemy",
]

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest"]

setup(
    author="Devon Shurick",
    author_email="dshurick@gmail.com",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="A Python package for dbt data collection and analysis.",
    entry_points={"console_scripts": ["sportsball=sportsball.cli:main"]},
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="sportsball",
    name="sportsball",
    packages=find_packages(include=["sportsball"]),
    setup_requires=setup_requirements,
    extras_require={
        "dev": ["pytest", "pytest-pep8", "pytest-cov", "invoke"],
        "docs": ["sphinx", "sphinxcontrib-napoleon"],
    },
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/dshurick/sportsball",
    version="0.1.0",
    zip_safe=False,
)
