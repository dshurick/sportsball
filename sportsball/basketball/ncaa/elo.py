import math

import pandas as pd
from sqlalchemy import create_engine


class Util(object):
    engine = create_engine("postgresql://dshurick@localhost/sports")

    def __init__(self):
        self.games = self.get_games()
        self.team_names = self.get_teams()
        self.teams = None

    def get_games(self):
        df = pd.read_sql_query(
            """
            SELECT
                season
              , daynum
              , team_1
              , team_2
              , season::TEXT || ':' || team_1::TEXT                                AS season_team_1
              , season::TEXT || ':' || team_2::TEXT                                AS season_team_2
              , minutes_played
              , row_number() OVER (PARTITION BY season, team_1 ORDER BY game_date) AS gm_num_1
              , row_number() OVER (PARTITION BY season, team_2 ORDER BY game_date) AS gm_num_2
              , points_1
              , points_2
              , coalesce(home_team_id::TEXT, '')                                   AS home_team_id
              , game_date
              , est_poss
              , CASE
                    WHEN home_team_id = team_1 THEN 1
                    WHEN home_team_id = team_2 THEN -1
                    ELSE 0 END                                                     AS team1home
              , tempo
              , off_eff_1
              , off_eff_2
              , def_eff_1
              , def_eff_2
            FROM ncaamb.efficiency_metrics
            WHERE NOT flipped
            ORDER BY game_date;
            """,
            con=self.engine,
            parse_dates="game_date",
        )

        df.sort_values("game_date", inplace=True)

        return sorted(df.to_dict(orient="records"), key=lambda x: x.get("game_date"))

    def get_teams(self):
        df = pd.read_sql_query(
            """
            SELECT
                  teamid
                , teamname
            FROM seed_data."Teams";
            """,
            con=self.engine,
        )
        teamnames = {
            team["teamid"]: team["teamname"] for team in df.to_dict(orient="records")
        }
        return teamnames

    def forecast(self):

        self.teams = {
            team_id: {
                "name": team_name,
                "season": None,
                "elo": 1300.0,
                # "ending_elos": dict(),
            }
            for team_id, team_name in self.team_names.items()
        }

        reversions = dict()
        elo_points_by_season = dict()

        revert = 1 / 4.46
        hfa = 58.6
        kk = 13.8
        denom = 224.8

        daily_ratings = []

        for game in self.games:

            id_1 = game.get("team_1")
            id_2 = game.get("team_2")

            team1, team2 = self.teams[id_1], self.teams[id_2]

            if game["season"] not in elo_points_by_season:
                elo_points_by_season[game["season"]] = 0.0

            for team in (team1, team2):
                if team["season"] and game["season"] != team["season"]:
                    # team["ending_elos"][team["season"]] = team["elo"]
                    k = "%s%s" % (team["name"], game["season"])
                    if k in reversions:
                        team["elo"] = reversions[k]
                    else:
                        team["elo"] = 1505.0 * revert + team["elo"] * (1 - revert)
                team["season"] = game["season"]

            game["elo_1"] = team1["elo"]
            game["elo_2"] = team2["elo"]

            # Elo difference includes home field advantage
            elo_diff = team1["elo"] - team2["elo"] + (game["team1home"] * hfa)

            # This is the most important piece, where we set my_prob1 to our forecasted
            # probability
            game["prob"] = 1.0 / (math.pow(10.0, (-elo_diff / denom)) + 1.0)

            # If game was played, maintain team Elo ratings
            if game["points_1"]:
                # Margin of victory is used as a K multiplier
                margin = abs(game["points_1"] - game["points_2"])

                game["result"] = 1.0 if game["points_1"] > game["points_2"] else 0.0

                mult = math.log(max(margin, 1) + 1.0) * (
                    2.2
                    / (
                        (elo_diff if game["points_1"] > game["points_2"] else -elo_diff)
                        * 0.001
                        + 2.2
                    )
                )

                elo_brier = (game["prob"] - game["result"]) ** 2
                elo_points = 25 - (100 * elo_brier)
                elo_points_by_season[game["season"]] += elo_points

                # Elo shift based on K and the margin of victory multiplier
                shift = (kk * mult) * (game["result"] - game["prob"])

                # Apply shift
                team1["elo"] += shift
                team2["elo"] -= shift

                daily_ratings.append(
                    dict(
                        name=team1["name"],
                        season=game["season"],
                        date=game["game_date"],
                        elo=team1["elo"],
                    )
                )
                daily_ratings.append(
                    dict(
                        name=team2["name"],
                        season=game["season"],
                        date=game["game_date"],
                        elo=team2["elo"],
                    )
                )

        return daily_ratings
