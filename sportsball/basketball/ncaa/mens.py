import csv
import datetime
import os

from smart_open import open

from sportsball.basketball.ncaa.classes import (
    Conference,
    Season,
    Game,
    Team,
    TeamSeason,
)


# Season,DayNum,WTeamID,WScore,LTeamID,LScore,WLoc,NumOT,WFGM,WFGA,WFGM3,WFGA3,WFTM,
# WFTA,WOR,WDR,WAst,WTO,WStl,WBlk,WPF,LFGM,LFGA,LFGM3,LFGA3,LFTM,LFTA,LOR,LDR,LAst,LTO,
# LStl,LBlk,LPF
def load_games(filepath, seasons, teams):
    games = []
    with open(filepath, "r") as games_file:
        dict_reader = csv.DictReader(games_file)
        for row in dict_reader:
            year = int(row.get("Season"))
            daynum = int(row.get("DayNum"))
            season = seasons.get(year)
            winning_team = teams.get(int(row.get("WTeamID")))
            losing_team = teams.get(int(row.get("LTeamID")))
            new_game = Game(
                season=season,
                date=season.start_date + datetime.timedelta(days=daynum),
                winning_team=winning_team,
                losing_team=losing_team,
            )
            games.append(new_game)
    return games


def load_seasons(filepath):
    seasons = dict()
    with open(filepath, "r") as seasons_file:
        dict_reader = csv.DictReader(seasons_file)
        for row in dict_reader:
            new_season = Season(
                year=row["Season"],
                start_date=row["DayZero"],
                region_w=row["RegionW"],
                region_x=row["RegionX"],
                region_y=row["RegionY"],
                region_z=row["RegionZ"],
            )
            seasons[new_season.year] = new_season
    return seasons


def load_conferences(filepath):
    conferences = dict()
    with open(filepath, "r") as conferences_file:
        dict_reader = csv.DictReader(conferences_file)
        for row in dict_reader:
            new_conference = Conference(
                abbrev=row["ConfAbbrev"], name=row["Description"]
            )
            conferences[new_conference.abbrev] = new_conference
    return conferences


def load_team_seasons(filepath, seasons, conferences, teams):
    """
    Load team to conference mapping from CSV file. Expects a file with columns `Season`,
    `TeamID`, and `ConfAbbrev`.

    :param filepath:
    :param seasons:
    :param conferences:
    :param teams:
    :return:
    """
    team_seasons = dict()
    with open(filepath, "r") as teams_file:
        dict_reader = csv.DictReader(teams_file)
        for row in dict_reader:
            season = seasons.get(int(row.get("Season")))
            conference = conferences.get(row.get("ConfAbbrev"))
            team_id = int(row.get("TeamID"))
            team = teams.get(team_id)

            new_team = TeamSeason(team, conference=conference, season=season)

            team_seasons[(season.year, team_id)] = new_team
    return team_seasons


def load_teams(filepath):
    """
    Load teams info from CSV file. Expects a file with columns `TeamID`,
    `TeamName`, `FirstD1Season` and `LastD1Season`.

    :param filepath:
    :return:
    """
    teams = dict()
    with open(filepath, "r") as teams_file:
        dict_reader = csv.DictReader(teams_file)
        for row in dict_reader:
            new_team = Team(id=int(row["TeamID"]), name=row["TeamName"])
            teams[new_team.id] = new_team
    return teams


def load_data(datapath):
    seasons = load_seasons(filepath=os.path.join(datapath, "Seasons.csv"))
    conferences = load_conferences(filepath=os.path.join(datapath, "Conferences.csv"))
    teams = load_teams(filepath=os.path.join(datapath, "Teams.csv"))

    team_seasons = load_team_seasons(
        filepath=os.path.join(datapath, "TeamConferences.csv"),
        seasons=seasons,
        conferences=conferences,
        teams=teams,
    )

    print(team_seasons)
