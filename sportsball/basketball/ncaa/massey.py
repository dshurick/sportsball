import re
from collections import defaultdict

import lxml

from sportsball.basketball.ncaa.mens import MasseyNCAAMB
from sportsball.basketball.ncaa.classes import Team, Game
from sportsball.sources.massey import MasseyAPI


class MasseySeason(object):
    def __init__(self, year):
        self.year = year
        self.teams = dict()
        self.games = list()
        self.__api = MasseyNCAAMB()
        self._division_team_map = defaultdict(set)

    def scrape_divisions(self):
        division2teams, team2division = self.__api.division_team_mappings(self.year)
        for teamstr, divisionstr in team2division.items():
            self.teams[teamstr] = Team(teamstr, divisionstr, season=self)
        for division_str, teams in division2teams.items():
            for teamstr in teams:
                self._division_team_map[division_str].add(self.teams[teamstr])

    def scrape_games(self, scheduled=True, exhibition=True):

        self.games = list()

        if not self.teams:
            self.scrape_divisions()

        req = self.__api.get_games(
            season=self.year,
            division="ALL",
            format="fw_text",
            mode="All",
            scheduled=scheduled,
            exhibition=exhibition,
        )

        root = lxml.html.soupparser.fromstring(req.content)
        main_text = root.find(".//pre").text
        for line in str.splitlines(main_text):
            match = self.__api.RE_GAME.search(line)
            if match:
                match_dict = match.groupdict()
                match_dict["team1"] = match_dict["team1"].strip()
                match_dict["team2"] = match_dict["team2"].strip()

                match_dict["note"] = match_dict["note"].strip()

                if match_dict["team1"] not in self.teams:
                    self.teams[match_dict["team1"]] = Team(
                        match_dict["team1"], season=self
                    )
                team1 = self.teams[match_dict["team1"]]

                if match_dict["team2"] not in self.teams:
                    self.teams[match_dict["team2"]] = Team(
                        match_dict["team2"], season=self
                    )
                team2 = self.teams[match_dict["team2"]]

                home_team = None
                if match_dict["homeind1"]:
                    home_team = self.teams[match_dict["team1"]]
                elif match_dict["homeind2"]:
                    home_team = self.teams[match_dict["team2"]]

                is_scheduled = match_dict.get("scheduled") is not None
                is_playoff = match_dict.get("playoff") is not None
                is_exhibition = match_dict.get("exhibition") is not None
                is_forfeit = match_dict.get("forfeit") is not None
                is_stricken = match_dict.get("stricken") is not None

                overtimes = (
                    None
                    if not match_dict.get("overtimes")
                    else int(match_dict.get("overtimes"))
                )

                kwargs = dict(
                    date=match_dict.get("date"),
                    team1=team1,
                    team2=team2,
                    score1=int(match_dict.get("score1")),
                    score2=int(match_dict.get("score2")),
                    home_team=home_team,
                    is_scheduled=is_scheduled,
                    is_playoff=is_playoff,
                    is_exhibition=is_exhibition,
                    is_forfeit=is_forfeit,
                    is_stricken=is_stricken,
                    overtimes=overtimes,
                )

                self.games.append(Game(**kwargs))


class MasseyNCAAMB(MasseyAPI):

    DIVISION_MAP = {
        "ALL": 11590,
        "Horizon": 3339,
        "America East": 10344,
        "Atlantic Coast": 10423,
        "Atlantic Sun": 10443,
        "Atlantic Ten": 10447,
        "Big East": 10636,
        "Big Sky": 10668,
        "Big South": 10672,
        "Big 10": 10678,
        "Big 12": 10686,
        "Big West": 10695,
        "Colonial": 11246,
        "Conference USA": 11312,
        "Ivy League": 12206,
        "Metro Atlantic": 12475,
        "Mid-American": 12526,
        "Mid-Eastern AC": 12545,
        "Missouri Val": 12684,
        "Mountain West": 12734,
        "Northeast": 13046,
        "OH Valley": 13193,
        "Patriot League": 13333,
        "Southeastern": 14028,
        "Southern": 14064,
        "Southland": 14112,
        "Southwestern AC": 14163,
        "Sun Belt": 14238,
        "West Coast": 14553,
        "Western Athletic": 14576,
        "Summit Lg": 74834,
        "Pac 12": 107818,
        "American Athletic": 200137,
    }

    SEASON_MAP = {
        1996: 41459,
        1997: 41460,
        1998: 41461,
        1999: 41462,
        2000: 41463,
        2001: 41464,
        2002: 41465,
        2003: 41466,
        2004: 41467,
        2005: 41468,
        2006: 41469,
        2007: 69587,
        2008: 74994,
        2009: 87798,
        2010: 97288,
        2011: 101140,
        2019: 305972,
        2020: 309912,
    }

    MODE_MAP = {"Inter": 1, "Intra": 2, "All": 3}

    FORMATS = {"fw_text": 0, "matlab_games": 1, "matlab_hyper_games": 3}

    RE_GAME = re.compile(
        r"".join(
            [
                "\A",
                "(?P<date>\d{4}-\d{2}-\d{2})",
                "\s+",
                "(?P<homeind1>@)?",
                "(?P<team1>\D+)",
                "(?P<score1>\d{1,3})",
                "\s+",
                "(?P<homeind2>@)?",
                "(?P<team2>\D+)",
                "(?P<score2>\d{1,3})",
                "\s",
                "(?P<scheduled>Sch)?",
                "(?P<playoff>P)?",
                "(?P<exhibition>E)?",
                "(?P<forfeit>F)?",
                "(?P<stricken>S)?",
                "(O(?P<overtimes>\d{1}))?",
                "\s+",
                "(?P<note>.*)",
                "\Z",
            ]
        )
    )

    def get_games(
        self,
        season,
        division="ALL",
        format="fw_text",
        mode="All",
        scheduled=False,
        exhibition=False,
    ):

        params = {
            "all": 1,
            "s": self.SEASON_MAP.get(season),
            "sub": self.DIVISION_MAP.get(division),
            "format": self.FORMATS.get(format),
            "mode": self.MODE_MAP.get(mode),
        }

        if scheduled:
            params["sch"] = "on"
        if exhibition:
            params["exhib"] = "on"

        if params.get("format") == 0:
            # This uses the fixed-width format
            return self._make_fw_text_request(params)
        elif params.get("format") == 1:
            return self._make_csv_request(
                params,
                names=[
                    "date_id",
                    "datestr",
                    "winner_id",
                    "home_adv_winner",
                    "winner_score",
                    "loser_id",
                    "home_adv_loser",
                    "loser_score",
                ],
            )
        elif params.get("format") == 3:
            return self._make_csv_request(
                params,
                names=[
                    "date_id",
                    "datestr",
                    "game_id",
                    "rownum",
                    "team_id",
                    "home_adv",
                    "team_score",
                ],
            )
        else:
            raise ValueError(
                "Expected 'format' to be one of: {}".format(list(self.FORMATS.keys()))
            )

    def division_teams(
        self, season, division="ALL", mode="All", scheduled=False, exhibition=False
    ):
        params = {
            "all": 1,
            "s": self.SEASON_MAP[season],
            "sub": self.DIVISION_MAP[division],
            "format": 2,
            "mode": self.MODE_MAP[mode],
        }

        if scheduled:
            params["sch"] = "on"
        if exhibition:
            params["exhib"] = "on"

        df = self._make_csv_request(params, names=["rownum", "team_name"])
        return set(map(str.strip, df.team_name))

    def division_team_mappings(self, season):
        division2teams = dict()
        team2division = dict()
        for division in self.DIVISION_MAP.keys():
            teams = self.division_teams(season=season, division=division)
            division2teams[division] = teams
            if division != "ALL":
                for team in teams:
                    team2division[team] = division
        return division2teams, team2division
