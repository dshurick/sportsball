import dateutil


class Team(object):
    def __init__(self, id, name):
        self.id = id
        self.name = name


class TeamSeason(object):
    def __init__(self, team, conference=None, season=None):
        self.team = team
        self.conference = conference
        self.season = season

    def __repr__(self):
        return f"<TeamSeason: {self.season.year}-{self.team.name}>"


class Game(object):
    def __init__(self, season, date, winning_team, losing_team):
        self.season = season
        self.date = date
        self.winning_team = winning_team
        self.losing_team = losing_team

    def __repr__(self):
        return "<Game: {date:%Y-%m-%d} {team1}[{score1}] vs. {team2}[{score2}]>".format(
            date=self.date,
            team1=self.team1.name,
            score1=self.score1,
            team2=self.team2.name,
            score2=self.score2,
        )


class Conference(object):
    def __init__(self, abbrev, name):
        self.abbrev = abbrev
        self.name = name

    def __repr__(self):
        return f"<Conference: {self.name}>"


class Coach(object):
    def __init__(self, name):
        self.name = name


class Season(object):
    def __init__(self, year, start_date, region_w, region_x, region_y, region_z):
        self.year = int(year)
        if isinstance(start_date, str):
            start_date = dateutil.parser.parse(start_date).date()
        self.start_date = start_date
        self.regions = dict(
            region_w=region_w, region_x=region_x, region_y=region_y, region_z=region_z
        )
