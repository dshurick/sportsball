import csv
import logging
import os
from contextlib import contextmanager

from dateutil.parser import parser
from smart_open import open
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sportsball.basketball.ncaa import models
from sportsball.sportsball import DATA_DIR

logger = logging.getLogger(__name__)
engine = create_engine("postgresql://dshurick@localhost/ncaamb")
Session = sessionmaker(bind=engine)


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def load_data(uri, model, fieldmapping, file_encoding=None, update=False):
    with open(uri, "r", encoding=file_encoding) as infile:
        dict_reader = csv.DictReader(infile)
        with session_scope() as session:
            for row in dict_reader:
                kwargs = {v: row.get(k) for k, v in fieldmapping.items()}
                if update:
                    session.merge(model(**kwargs))
                else:
                    session.add(model(**kwargs))


def load_seasons(uri, model, fieldmapping, file_encoding=None, update=False):
    with open(uri, "r", encoding=file_encoding) as infile:
        dict_reader = csv.DictReader(infile)
        with session_scope() as session:
            for row in dict_reader:
                kwargs = {v: row.get(k) for k, v in fieldmapping.items()}
                kwargs["day_zero"] = parser.parse(kwargs["day_zero"]).date()
                if update:
                    session.merge(model(**kwargs))
                else:
                    session.add(model(**kwargs))


def load_games():
    for filename, gametype in (
        ("RegularSeasonCompactResults.csv", "Regular"),
        ("NCAATourneyCompactResults.csv", "NCAA"),
        ("SecondaryTourneyCompactResults.csv", "Secondary"),
    ):
        with open(os.path.join(DATA_DIR, "prepared", filename), "r") as infile:
            dict_reader = csv.DictReader(infile)
            with session_scope() as session:
                for row in dict_reader:
                    wteam = int(row.get("WTeamID"))
                    lteam = int(row.get("LTeamID"))

                    home_team = None
                    if row["WLoc"] == "H":
                        home_team = wteam
                    if row["WLoc"] == "A":
                        home_team = lteam
                    game = models.Game(
                        season=row.get("Season"),
                        day_num=row.get("DayNum"),
                        team_id1=wteam,
                        team_id2=lteam,
                        winning_team=wteam,
                        home_team=home_team,
                        gametype=gametype,
                        numot=row.get("NumOT"),
                        points_tm1=row.get("WScore"),
                        points_tm2=row.get("LScore"),
                    )
                    session.add(game)

    for filename in (
        "RegularSeasonDetailedResults.csv",
        "NCAATourneyDetailedResults.csv",
    ):
        with open(os.path.join(DATA_DIR, "prepared", filename), "r") as infile:
            dict_reader = csv.DictReader(infile)
            with session_scope() as session:
                for row in dict_reader:
                    session.add(
                        models.GameStats(
                            season=row.get("Season"),
                            day_num=row.get("DayNum"),
                            team_id=row.get("WTeamID"),
                            field_goals_made=row.get("WFGM"),
                            field_goals_att=row.get("WFGA"),
                            threes_made=row.get("WFGM3"),
                            threes_att=row.get("WFGA3"),
                            free_throws_made=row.get("WFTM"),
                            free_throws_att=row.get("WFTA"),
                            off_reb=row.get("WOR"),
                            def_reb=row.get("WDR"),
                            assists=row.get("WAst"),
                            turnovers=row.get("WTO"),
                            steals=row.get("WStl"),
                            blocks=row.get("WBlk"),
                            personal_fouls=row.get("WPF"),
                        )
                    )

                    session.add(
                        models.GameStats(
                            season=row.get("Season"),
                            day_num=row.get("DayNum"),
                            team_id=row.get("LTeamID"),
                            field_goals_made=row.get("LFGM"),
                            field_goals_att=row.get("LFGA"),
                            threes_made=row.get("LFGM3"),
                            threes_att=row.get("LFGA3"),
                            free_throws_made=row.get("LFTM"),
                            free_throws_att=row.get("LFTA"),
                            off_reb=row.get("LOR"),
                            def_reb=row.get("LDR"),
                            assists=row.get("LAst"),
                            turnovers=row.get("LTO"),
                            steals=row.get("LStl"),
                            blocks=row.get("LBlk"),
                            personal_fouls=row.get("LPF"),
                        )
                    )


if __name__ == "__main__":

    models.Base.metadata.drop_all(engine)
    models.Base.metadata.create_all(engine)

    logger.info("Loading Seasons.csv ...")
    load_data(
        uri=os.path.join(DATA_DIR, "prepared", "Seasons.csv"),
        model=models.Season,
        fieldmapping={
            "Season": "id",
            "DayZero": "day_zero",
            "RegionW": "region_w",
            "RegionX": "region_x",
            "RegionY": "region_y",
            "RegionZ": "region_z",
        },
        update=False,
    )

    logger.info("Loading Teams.csv ...")
    load_data(
        uri=os.path.join(DATA_DIR, "prepared", "Teams.csv"),
        model=models.Team,
        fieldmapping={"TeamID": "id", "TeamName": "name"},
        update=False,
    )

    logger.info("Loading TeamSpellings.csv ...")
    load_data(
        uri=os.path.join(DATA_DIR, "prepared", "TeamSpellings.csv"),
        model=models.TeamSpelling,
        fieldmapping={"TeamNameSpelling": "team_spelling", "TeamID": "team_id"},
        file_encoding="ISO-8859-1",
        update=False,
    )

    logger.info("Loading Conferences.csv ...")
    load_data(
        uri=os.path.join(DATA_DIR, "prepared", "Conferences.csv"),
        model=models.Conference,
        fieldmapping={"ConfAbbrev": "id", "Description": "name"},
        update=False,
    )

    logger.info("Loading TeamConferences.csv ...")
    load_data(
        uri=os.path.join(DATA_DIR, "prepared", "TeamConferences.csv"),
        model=models.TeamConference,
        fieldmapping={
            "Season": "season",
            "TeamID": "team_id",
            "ConfAbbrev": "conference_id",
        },
        update=False,
    )

    logger.info("Loading game data...")
    load_games()
