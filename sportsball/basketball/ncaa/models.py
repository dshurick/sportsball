from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects import postgresql
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Team(Base):
    __tablename__ = "team"

    id = Column(postgresql.INTEGER, primary_key=True)
    name = Column(postgresql.TEXT)

    spellings = relationship("TeamSpelling", back_populates="team")


class Season(Base):
    __tablename__ = "season"

    id = Column(postgresql.INTEGER, primary_key=True)
    day_zero = Column(postgresql.DATE)
    region_w = Column(postgresql.TEXT)
    region_x = Column(postgresql.TEXT)
    region_y = Column(postgresql.TEXT)
    region_z = Column(postgresql.TEXT)


class TeamSpelling(Base):
    __tablename__ = "team_spelling"

    id = Column(postgresql.INTEGER, primary_key=True)
    team_id = Column(postgresql.INTEGER, ForeignKey("team.id"))
    team_spelling = Column(postgresql.TEXT)

    team = relationship("Team", back_populates="spellings")


class TeamConference(Base):
    __tablename__ = "team_conference"

    id = Column(postgresql.INTEGER, primary_key=True)
    season = Column(postgresql.INTEGER)
    team_id = Column(postgresql.INTEGER, ForeignKey("team.id"))
    conference_id = Column(postgresql.TEXT, ForeignKey("conference.id"))

    conference = relationship("Conference", back_populates="team_conferences")


class Conference(Base):
    __tablename__ = "conference"

    id = Column(postgresql.TEXT, primary_key=True)
    name = Column(postgresql.TEXT)

    team_conferences = relationship("TeamConference", back_populates="conference")


class Game(Base):
    __tablename__ = "game"

    season = Column(ForeignKey(Season.id), primary_key=True)
    day_num = Column(postgresql.SMALLINT, primary_key=True)
    team_id1 = Column(ForeignKey(Team.id), primary_key=True)
    team_id2 = Column(ForeignKey(Team.id), primary_key=True)
    winning_team = Column(ForeignKey(Team.id))
    home_team = Column(ForeignKey(Team.id))
    # wloc = Column(ENUM("N", "A", "H", name="loc_enum", create_type=False))
    gametype = Column(
        ENUM(
            "Regular",
            "Secondary",
            "ConferenceTourney",
            "NCAA",
            name="gametype_enum",
            create_type=False,
        )
    )
    numot = Column(postgresql.SMALLINT)
    points_tm1 = Column(postgresql.SMALLINT)
    points_tm2 = Column(postgresql.SMALLINT)


class GameStats(Base):
    __tablename__ = "game_stats"

    season = Column(ForeignKey(Season.id), primary_key=True)
    day_num = Column(postgresql.SMALLINT, primary_key=True)
    team_id = Column(ForeignKey(Team.id), primary_key=True)

    field_goals_made = Column(postgresql.SMALLINT)
    field_goals_att = Column(postgresql.SMALLINT)
    threes_made = Column(postgresql.SMALLINT)
    threes_att = Column(postgresql.SMALLINT)
    free_throws_made = Column(postgresql.SMALLINT)
    free_throws_att = Column(postgresql.SMALLINT)
    off_reb = Column(postgresql.SMALLINT)
    def_reb = Column(postgresql.SMALLINT)
    assists = Column(postgresql.SMALLINT)
    turnovers = Column(postgresql.SMALLINT)
    steals = Column(postgresql.SMALLINT)
    blocks = Column(postgresql.SMALLINT)
    personal_fouls = Column(postgresql.SMALLINT)
