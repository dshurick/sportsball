# -*- coding: utf-8 -*-
"""Main module."""
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.abspath(os.path.join(ROOT_DIR, os.path.pardir, "data"))
