import urllib

import pandas as pd
import requests


class MasseyAPI(object):

    API_ROOT = "https://www.masseyratings.com/scores.php"

    def __init__(self):
        self.session = requests.Session()
        self.session.headers = {"application": "PythonWrapper"}

    def _make_csv_request(self, params, names=None):

        return pd.read_csv(
            "{root}?{query}".format(
                root=self.API_ROOT, query=urllib.parse.urlencode(params)
            ),
            names=names,
            header=None,
        )

    def _make_fw_text_request(self, params):
        return self.session.request("GET", self.API_ROOT, params=params)
