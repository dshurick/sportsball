# -*- coding: utf-8 -*-
"""Top-level package for sportsball."""
import logging

__author__ = """Devon Shurick"""
__email__ = "dshurick@gmail.com"
__version__ = "0.1.0"


logging.basicConfig(level=logging.DEBUG)
