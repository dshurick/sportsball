==========
sportsball
==========


.. image:: https://gitlab.com/dshurick/sportsball/badges/feature/ncaamb-compile-data/pipeline.svg
        :target: https://gitlab.com/dshurick/sportsball/commits/feature/ncaamb-compile-data

.. image:: https://readthedocs.org/projects/sportsball/badge/?version=latest
        :target: https://sportsball.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/dshurick/sportsball/shield.svg
     :target: https://pyup.io/repos/github/dshurick/sportsball/
     :alt: Updates



A Python package for sports data collection and analysis.


* Free software: MIT license
* Documentation: https://sportsball.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
