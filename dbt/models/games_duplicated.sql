{{ config(schema='ncaamb') }}

with source as (select * from {{ ref('RegularSeasonDetailedResults') }})
   , seasons AS (select * from {{ ref('Seasons') }})
   , games_stacked AS (
    select
        season
      , daynum
      , wteamid        as team_1
      , lteamid        as team_2
      , 40 + numot * 5 as minutes_played
      , wscore            as points_1
      , lscore            as points_2
      , case
            when wloc = 'H' then wteamid
            when wloc = 'A' then lteamid
            else null end as home_team_id
      , wfgm           as fgm_1
      , wfga           as fga_1
      , wfgm3          as fgm3_1
      , wfga3          as fga3_1
      , wftm           as ftm_1
      , wfta           as fta_1
      , wor            as or_1
      , wdr            as dr_1
      , wast           as ast_1
      , wto            as to_1
      , wstl           as stl_1
      , wblk           as blk_1
      , wpf            as pf_1
      , lfgm           as fgm_2
      , lfga           as fga_2
      , lfgm3          as fgm3_2
      , lfga3          as fga3_2
      , lftm           as ftm_2
      , lfta           as fta_2
      , lor            as or_2
      , ldr            as dr_2
      , last           as ast_2
      , lto            as to_2
      , lstl           as stl_2
      , lblk           as blk_2
      , lpf            as pf_2
      , FALSE          as flipped
    FROM source
    UNION ALL
    SELECT
        season
      , daynum
      , lteamid        as team_1
      , wteamid        as team_2
      , 40 + numot * 5 as minutes_played
      , lscore            as points_1
      , wscore            as points_2
      , case
            when wloc = 'H' then wteamid
            when wloc = 'A' then lteamid
            else null end as home_team_id
      , lfgm           as fgm_1
      , lfga           as fga_1
      , lfgm3          as fgm3_1
      , lfga3          as fga3_1
      , lftm           as ftm_1
      , lfta           as fta_1
      , lor            as or_1
      , ldr            as dr_1
      , last           as ast_1
      , lto            as to_1
      , lstl           as stl_1
      , lblk           as blk_1
      , lpf            as pf_1
      , wfgm           as fgm_2
      , wfga           as fga_2
      , wfgm3          as fgm3_2
      , wfga3          as fga3_2
      , wftm           as ftm_2
      , wfta           as fta_2
      , wor            as or_2
      , wdr            as dr_2
      , wast           as ast_2
      , wto            as to_2
      , wstl           as stl_2
      , wblk           as blk_2
      , wpf            as pf_2
      , TRUE           as flipped
    FROM source)

SELECT
    games_stacked.*
  , (seasons.dayzero + games_stacked.daynum)::DATE AS game_date
FROM games_stacked
         LEFT JOIN seasons
                   ON games_stacked.season = seasons.season