{{ config(schema='ncaamb') }}

WITH source AS (SELECT * FROM {{ ref('games_duplicated') }})
   , est_poss AS (
    SELECT
        source.*
      , source.fga_1 - source.or_1 + source.to_1 + 0.475 * source.fta_1         AS est_poss_1
      , source.fga_2 - source.or_2 + source.to_2 + 0.475 * source.fta_2         AS est_poss_2
      , 0.5 * (source.fga_1 - source.or_1 + source.to_1 + 0.475 * source.fta_1) +
        0.5 * (source.fga_2 - source.or_2 + source.to_2 + 0.475 * source.fta_2) AS est_poss
    FROM source)
SELECT
    est_poss.*
  , 40 * est_poss.est_poss / est_poss.minutes_played AS tempo
  , 100 * est_poss.points_1 / est_poss.est_poss      AS off_eff_1
  , 100 * est_poss.points_2 / est_poss.est_poss      AS off_eff_2
  , 100 * est_poss.points_2 / est_poss.est_poss      AS def_eff_1
  , 100 * est_poss.points_1 / est_poss.est_poss      AS def_eff_2
FROM est_poss