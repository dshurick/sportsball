#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Tests for `sportsball` package."""

from datetime import date

from click.testing import CliRunner

from sportsball import cli
from sportsball.basketball.ncaa.mens import Season, Team, Game


def test_season():
    season = Season(2020)
    season.scrape_divisions()
    season.scrape_games(scheduled=False, exhibition=False)


def test_game():
    season = Season(2020)
    team1 = Team("team1", conference="divisionA", season=season)
    team2 = Team("team2", conference="divisionB", season=season)
    game = Game(date=str(date.today()), team1=team1, team2=team2, score1=34, score2=57)

    assert game.team1.name == "team1"
    assert game.team2.name == "team2"


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert "sportsball.cli.main" in result.output
    help_result = runner.invoke(cli.main, ["--help"])
    assert help_result.exit_code == 0
    assert "--help  Show this message and exit." in help_result.output
